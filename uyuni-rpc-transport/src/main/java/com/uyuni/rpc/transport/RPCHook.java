package com.uyuni.rpc.transport;

import com.uyuni.rpc.transport.model.RemotingTransporter;

/**
 *
 * @author zhanghailin
 * @description RPC的回调钩子，在发送请求和接收请求的时候触发，这样做事增加程序的健壮性和灵活性
 * @since 2018-07-27
 */
public interface RPCHook {
	
	void doBeforeRequest(final String remoteAddr, final RemotingTransporter request);

    void doAfterResponse(final String remoteAddr, final RemotingTransporter request, final RemotingTransporter response);

}
