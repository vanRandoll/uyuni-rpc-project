package com.uyuni.rpc.transport.watcher;

import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class TimeTest {

    public static void main(String[] args) {
        //new Reminder(3);
        io.netty.util.Timer timer = new HashedWheelTimer();
        timer.newTimeout(new TimeTaskDemo(),2, TimeUnit.MILLISECONDS);
    }

    public static class Reminder {
        Timer timer ;
        public Reminder(int sec) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    System.out.println("Time's up!");
                    timer.cancel();
                }
            },sec*1000);
        }
    }

    public static class TimeTaskDemo implements io.netty.util.TimerTask {
        @Override
        public void run(Timeout timeout) throws Exception {
            System.out.println(1);
        }
    }
}
