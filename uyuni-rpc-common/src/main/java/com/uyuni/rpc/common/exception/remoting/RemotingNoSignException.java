package com.uyuni.rpc.common.exception.remoting;

/**
 * 
 * @author zhanghailin
 * @description
 * @time
 * @modifytime
 */
public class RemotingNoSignException extends RemotingException {

	private static final long serialVersionUID = -1661779813708564404L;


	public RemotingNoSignException(String message) {
        super(message, null);
    }


    public RemotingNoSignException(String message, Throwable cause) {
        super(message, cause);
    }

}
