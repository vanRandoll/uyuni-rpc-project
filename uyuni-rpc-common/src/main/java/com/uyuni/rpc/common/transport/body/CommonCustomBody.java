package com.uyuni.rpc.common.transport.body;

import com.uyuni.rpc.common.exception.remoting.RemotingCommmonCustomException;

public interface CommonCustomBody {

    void checkFields() throws RemotingCommmonCustomException;
}
