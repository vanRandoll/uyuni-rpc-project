package com.uyuni.rpc.common.serialization.proto;

import java.util.List;

public class Student {

    private int age;

    private String name;

    private List<String> interesting;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getInteresting() {
        return interesting;
    }

    public void setInteresting(List<String> interesting) {
        this.interesting = interesting;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", interesting=" + interesting +
                '}';
    }
}
