package com.uyuni.rpc.common.serialization;

/**
 * 
 * @author zhanghailin
 * @description 序列化接口
 */
public interface Serializer {

	/**
	 * 将对象序列化成byte[]
	 * @param obj 序列化对象
	 * @return 返回字节对象
	 */
    <T> byte[] writeObject(T obj);

    /**
     * 将byte数组反序列成对象
     * @param bytes 字节对象
     * @param clazz 序列化的class
     * @return 返回原始对象
     */
    <T> T readObject(byte[] bytes, Class<T> clazz);
}
