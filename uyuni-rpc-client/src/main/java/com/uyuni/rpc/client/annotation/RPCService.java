package com.uyuni.rpc.client.annotation;

import java.lang.annotation.*;

/**
 * @author zhl
 * @description 服务提供端提供服务的Annotation
 * @since 2018-07-31
 * @modifytime
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface RPCService {

    /**
     * 服务名
     */
    String serviceName() default "";

    /**
     * 负载访问权重
     */
    int weight() default 50;

    /**
     * 负责人名
     */
    String responsibilityName() default "system";

    /**
     * 单实例连接数，注册中心该参数有效，直连无效
     */
    int connCount() default 1;

    /**
     * 是否是VIP服务
     */
    boolean isVIPService() default false;

    /**
     * 是否支持降级
     */
    boolean isSupportDegradeService() default false;

    /**
     * 服务名
     */
    String degradeServicePath() default "";

    /**
     * 降级服务的描述
     */
    String degradeServiceDesc() default "";

    /**
     * 是否单位时间限流
     */
    boolean isFlowController() default true;

    /**
     * 单位时间的最大调用量
     */
    long maxCallCountInMinute() default 100000;

}
