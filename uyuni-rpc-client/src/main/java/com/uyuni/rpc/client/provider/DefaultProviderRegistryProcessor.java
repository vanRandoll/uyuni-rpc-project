package com.uyuni.rpc.client.provider;

import com.uyuni.rpc.transport.ConnectionUtils;
import com.uyuni.rpc.transport.model.NettyRequestProcessor;
import com.uyuni.rpc.transport.model.RemotingTransporter;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.uyuni.rpc.common.protocol.UyuniProtocol.AUTO_DEGRADE_SERVICE;
import static com.uyuni.rpc.common.protocol.UyuniProtocol.DEGRADE_SERVICE;

/**
 * 
 * @author BazingaLyn
 * @description provider端注册的处理器
 * @time
 * @modifytime
 */
public class DefaultProviderRegistryProcessor implements NettyRequestProcessor {
	
	private static final Logger logger = LoggerFactory.getLogger(DefaultProviderRegistryProcessor.class);
	
	private DefaultProvider defaultProvider;

	public DefaultProviderRegistryProcessor(DefaultProvider defaultProvider) {
		this.defaultProvider = defaultProvider;
	}

	@Override
	public RemotingTransporter processRequest(ChannelHandlerContext ctx, RemotingTransporter request) throws Exception {
		
		if (logger.isDebugEnabled()) {
			logger.debug("receive request, {} {} {}",//
                request.getCode(), //
                ConnectionUtils.parseChannelRemoteAddr(ctx.channel()), //
                request);
        }

        //只处理降级请求
		switch (request.getCode()) {
		   case DEGRADE_SERVICE:
			    return this.defaultProvider.handlerDegradeServiceRequest(request,ctx.channel(),DEGRADE_SERVICE);
		   case AUTO_DEGRADE_SERVICE:
			    return this.defaultProvider.handlerDegradeServiceRequest(request,ctx.channel(),AUTO_DEGRADE_SERVICE);
		}
		return null;
	}

}
