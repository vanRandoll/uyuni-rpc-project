package com.uyuni.rpc.client.provider.model;

import com.uyuni.rpc.client.provider.DefaultProvider;
import com.uyuni.rpc.transport.model.NettyChannelInactiveProcessor;
import io.netty.channel.ChannelHandlerContext;

/**
 * 
 * @author BazingaLyn
 * @description provider的netty inactive触发的事件
 * @time
 * @modifytime
 */
public class DefaultProviderInactiveProcessor implements NettyChannelInactiveProcessor {

	private DefaultProvider defaultProvider;

	public DefaultProviderInactiveProcessor(DefaultProvider defaultProvider) {
		this.defaultProvider = defaultProvider;
	}

	@Override
	public void processChannelInactive(ChannelHandlerContext ctx) {
		defaultProvider.setProviderStateIsHealthy(false);
	}

}
