package com.uyuni.rpc.client.provider.flow.control;

import com.uyuni.rpc.common.utils.SystemClock;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 
 * @author BazingaLyn
 * @description 服务限流控制器
 * @time
 * @modifytime
 */
public class ServiceFlowController {

	private AtomicLong[] metricses = new AtomicLong[]{new AtomicLong(0L), new AtomicLong(0L), new AtomicLong(0L)};

	public long incrementOfCurrentMinute() {
		long currentTime = SystemClock.millisClock().now();
		int index = (int) ((currentTime / 60000) % 3);
		AtomicLong atomicLong = metricses[index];
		return atomicLong.incrementAndGet();

	}

	/**
	 * 当前分钟服务调用次数
	 * @return
	 */
	public long getCallCountOfCurrentMinute() {
		long currentTime = SystemClock.millisClock().now();
		int index = (int) (((currentTime / 60000)) % 3);
		AtomicLong atomicLong = metricses[index];
		return atomicLong.get();

	}

	/**
	 * 获取上一分钟的服务调用次数
	 * @return
	 */
	public long getCallCountOfLastMinute() {
		long currentTime = SystemClock.millisClock().now();
		int index = (int) (((currentTime / 60000) - 1) % 3);
		AtomicLong atomicLong = metricses[index];
		return atomicLong.get();

	}

	/**
	 * 获取下一分钟的服务调用次数
	 * @return
	 */
	public long getCallCountOfNextMinute() {
		long currentTime = SystemClock.millisClock().now();
		int index = (int) (((currentTime / 60000) + 1) % 3);
		AtomicLong atomicLong = metricses[index];
		return atomicLong.get();

	}

	public void clearNextMinuteCallCount() {
		long currentTime = SystemClock.millisClock().now();
		int index = (int) (((currentTime / 60000) + 1) % 3);
		AtomicLong atomicLong = metricses[index];
		atomicLong.set(0);
	}

	public AtomicLong[] getMetricses() {
		return metricses;
	}

	public void setMetricses(AtomicLong[] metricses) {
		this.metricses = metricses;
	}

}
