package com.uyuni.rpc.client.provider;

import com.uyuni.rpc.client.metrics.ServiceMeterManager;
import com.uyuni.rpc.client.provider.flow.control.ServiceFlowControllerManager;
import com.uyuni.rpc.client.provider.DefaultServiceProviderContainer.CurrentServiceState;
import com.uyuni.rpc.client.provider.model.ServiceWrapper;
import com.uyuni.rpc.common.utils.Pair;

import java.util.List;

/**
 * @author zhl
 * @description provider端的注册控制器
 * @since 2018-07-31
 */
public class ProviderRegistryController {

    private DefaultProvider defaultProvider;

    //provider与注册中心的所有逻辑控制器
    private RegistryController registryController;
    //provider与monitor端通信的控制器
    //private ProviderMonitorController providerMonitorController;
    //本地服务编织服务管理
    private LocalServerWrapperManager localServerWrapperManager;
    private final ServiceProviderContainer providerContainer;
    private ServiceFlowControllerManager serviceFlowControllerManager = new ServiceFlowControllerManager();

    public ProviderRegistryController(DefaultProvider defaultProvider) {
        this.defaultProvider = defaultProvider;
        providerContainer = new DefaultServiceProviderContainer();
        localServerWrapperManager = new LocalServerWrapperManager(this);
        registryController = new RegistryController(defaultProvider);
        //providerMonitorController = new ProviderMonitorController(defaultProvider);
    }

    /**
     * 检查服务是否需要降级
     */
    public void checkAutoDegrade() {
        //获取到所有需要降级的服务名
        List<Pair<String, CurrentServiceState>> needDegradeServices = providerContainer.getNeedAutoDegradeService();
        needDegradeServices.forEach(pair -> {
            //服务名
            String serviceName = pair.getKey();
            //最低成功率
            Integer minSuccessRate = pair.getValue().getMinSuccecssRate();
            //调用的实际成功率
            Integer realSuccessRate = ServiceMeterManager.calcServiceSuccessRate(serviceName);
            //降级条件满足 设置服务当前状态为
            final Pair<CurrentServiceState, ServiceWrapper> _pair = this.defaultProvider.getProviderRegistryController().getProviderContainer()
                    .lookupService(serviceName);
            CurrentServiceState currentServiceState = _pair.getKey();
            if (minSuccessRate > realSuccessRate
                    && (!currentServiceState.getHasDegrade().get())) {
                //服务降级
                currentServiceState.getHasDegrade().set(true);
            } else {
                //服务恢复
                currentServiceState.getHasDegrade().set(false);
            }
        });
    }

    public DefaultProvider getDefaultProvider() {
        return defaultProvider;
    }

    public void setDefaultProvider(DefaultProvider defaultProvider) {
        this.defaultProvider = defaultProvider;
    }

    public RegistryController getRegistryController() {
        return registryController;
    }

    public void setRegistryController(RegistryController registryController) {
        this.registryController = registryController;
    }

    public LocalServerWrapperManager getLocalServerWrapperManager() {
        return localServerWrapperManager;
    }

    public void setLocalServerWrapperManager(LocalServerWrapperManager localServerWrapperManager) {
        this.localServerWrapperManager = localServerWrapperManager;
    }

    public ServiceProviderContainer getProviderContainer() {
        return providerContainer;
    }

    public ServiceFlowControllerManager getServiceFlowControllerManager() {
        return serviceFlowControllerManager;
    }

    public void setServiceFlowControllerManager(ServiceFlowControllerManager serviceFlowControllerManager) {
        this.serviceFlowControllerManager = serviceFlowControllerManager;
    }
}
